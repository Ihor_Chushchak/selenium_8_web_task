Feature: Delete last message

    Scenario Outline: Delete last message
		Given User is on LogIn Gmail page
       	When User enters "<UserName>" and "<Password>"
       	And sends Email
       	And User delete last sent Email
       	Then User verifies whether last sent Email is deleted
		Examples:
		| UserName    						| Password   |
		| ihor.chushchak.kn.2017@lpnu.ua	| 14.06.2000 |
		| ihor.chushchak.kn.2017@lpnu.ua	| 14.06.2000 |
		| ihor.chushchak.kn.2017@lpnu.ua	| 14.06.2000 |
		| ihor.chushchak.kn.2017@lpnu.ua	| 14.06.2000 |
		| ihor.chushchak.kn.2017@lpnu.ua	| 14.06.2000 |


