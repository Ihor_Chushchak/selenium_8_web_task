package po.listener;

import com.google.common.io.Files;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;
import po.GmailPageTest;
import utils.DriverManager;

import java.io.File;
import java.io.IOException;

public class TestListener implements ITestListener {

    private static final Logger LOG = LogManager.getLogger(GmailPageTest.class);

    private static String getTestMethodName(ITestResult result){
        return result.getMethod().getConstructorOrMethod().getName();
    }

    @Attachment(type = "image/png")
    public static byte[] makeScreenshot(WebDriver driver)/* throws IOException */ {
        try {
            File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            return Files.toByteArray(screen);
        } catch (IOException e) {
            return null;
        }
    }

    @Attachment(value = "{0}", type = "text/plain")
    private String makeTextLog(String log) {
        return log;
    }

    @Override
    public void  onTestFailure(ITestResult result){
        LOG.info("Test Failure!!! Screenshot captured for test case: "
                + getTestMethodName(result));
        makeScreenshot(DriverManager.getDriver());
        makeTextLog(getTestMethodName(result) + " and screenshot taken!");
    }
}
