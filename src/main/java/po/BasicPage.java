package po;

import decorator.BasicElement;
import decorator.CustomFieldDecorator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.ConfigReader;
import utils.DriverManager;

public class BasicPage {

    public static WebDriver driver;
    WebDriverWait wait;

    BasicPage() {
        driver = DriverManager.getDriver();
        wait = new WebDriverWait(driver, Integer.parseInt(ConfigReader.read("DEFAULT_IMPLICITLY_WAIT_TIME")));
        PageFactory.initElements(new CustomFieldDecorator(new DefaultElementLocatorFactory(driver)), this);
    }
}
