package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;
import utils.WaitUtils;

public class CustomTextInput extends BasicElement {

    public CustomTextInput(WebElement webElement) {
        super(webElement);
    }
    public void sendKeys(String text) {
        WaitUtils.waitElementToBeClickable(webElement);
        super.sendKeys(text);
    }
}