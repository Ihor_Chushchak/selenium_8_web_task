package decorator;

import java.lang.reflect.Field;

import decorator.elements.CustomButton;
import decorator.elements.CustomLabel;
import decorator.elements.CustomTextInput;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

public class CustomFieldDecorator extends DefaultFieldDecorator {


    public CustomFieldDecorator(ElementLocatorFactory factory) {
        super(factory);
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);
        if (CustomButton.class.isAssignableFrom(field.getType())) {
            return new CustomButton(proxyForLocator(loader, locator));
        } else if (CustomTextInput.class.isAssignableFrom(field.getType())) {
            return new CustomTextInput(proxyForLocator(loader, locator));
        } else if (CustomLabel.class.isAssignableFrom(field.getType())) {
            return new CustomLabel(proxyForLocator(loader, locator));
        } else {
            return super.decorate(loader, field);
        }
    }
}