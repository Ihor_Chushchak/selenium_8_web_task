package bo;

import po.AuthorizationPage;

public class AuthorizationPageBO {

    private AuthorizationPage authorizationPage;

    public AuthorizationPageBO(){
        authorizationPage = new AuthorizationPage();
    }

    public void enterLoginToGmailPage(String login){
        authorizationPage.fillLogin(login);
        authorizationPage.clickLoginNextButton();
    }

    public void enterPasswordToGmailPage(String password){
        authorizationPage.fillPassword(password);
        authorizationPage.clickPasswordNextButton();
    }
}
